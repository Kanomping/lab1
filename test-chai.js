const chai = require('chai');
const expect = chai.expect;
const math = require('./math');
describe('Test chai', ()=> {
    it('should compare thing by expect', ()=>{
        expect(1).to.equal(1);
    });
    it('should compare another things by expect', () =>{
        expect(5>8).to.be.false;
        expect({name:'kob'}).to.deep.equal({name: 'kob'});
        expect({name:'kob'}).to.have.property('name').to.equal('kob');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('kob').to.be.a('String');
        expect('kob'.length).to.equal(3);
        expect('kob').to.lengthOf(3);
        expect([1,2,3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});

describe('Math module', ()=>{
    context('Function add1',()=>{
        it('should back all number', ()=>{
            expect(math.add1(0,0)).to.be.a('number');
        });
        it('add(1, 1) should sent value back is 2', ()=>{
            expect(math.add1(1,1)).to.equal(2);
        });


    });
});